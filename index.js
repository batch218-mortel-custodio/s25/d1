// JSON Objects

/*

- a common use of JSON is to read data from a web server, and display the data in the webpage
- it is a light-weight data-interchange format
-it is easy to read and write
- it is easy for machines to parse and generate


*/

// - json also use the key/value pairs just like the object properties in JS
// key/property names requires to be enclosed with double quotes

/*
	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "value"
	}
*/

/*{
	"city": "QC",
	"province": "MM",
	"country": "PH"
}*/

let batchArr = [
	{
		batchName: "Batch 218",
		schedule: "Part Time"
	},
	{
		batchName: "Batch 213",
		schedule: "Full Time"
	},
	{
		batchName: "Batch 216",
		schedule: "Full Time"
	}
]

console.log(batchArr);

console.log("Result from STRINGIFY method: ")

console.log(JSON.stringify(batchArr));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data);

/*let firstName = prompt("Enter your first name: ")
let lastName = prompt("Enter your last name: ")
let email = prompt("Enter your email: ")
let password = prompt("Enter your password: ")



let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	email: email,
	password: password
})

console.log(otherData);*/

let batchesJSON = `[
	{
		"batchName": "Batch 218",
		"schedule": "Part Time"
	},
	{
		"batchName": "Batch 213",
		"schedule": "Full Time"
	}
]`

console.log("batchesJSON content ")
console.log(batchesJSON);

console.log("Result from PARSE method: ")
console.log(JSON.parse(batchesJSON));


let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `[
	{
		"name": "John",
		"age": 31,
		"address" : {
			"city": "Manila",
			"country": "PH"
		}
	}
]`

console.log(stringifiedObject);

// convert stringifiedObject to a JS Object
console.log(JSON.parse(stringifiedObject));